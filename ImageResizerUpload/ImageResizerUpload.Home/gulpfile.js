﻿/// <binding AfterBuild='build-spa' />
var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var browserify = require('browserify');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var merge = require('merge');
var fs = require("fs");
var del = require('del');
var path = require('path');
var less = require('gulp-less');
var cssmin = require('gulp-cssmin');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
//var babel = require('gulp-babel');
var babel = require('babelify');
var _ = require('lodash');
var gulpWrapper = require('gulp-wrapper');
var gulpHeaderfooter = require('gulp-headerfooter');
var gulpConcat = require('gulp-concat');
var mergeStream = require('merge-stream');
var gulpPlumber = require('gulp-plumber');
var gulpUtil = require('gulp-util');

gulp.task('clean-lib', function () {
    return del(["./wwwroot/lib/", "./wwwroot/dist/"]);
});

gulp.task('concat-libs', function (done) {
    return gulp.src([
        "./wwwroot/lib/moment/moment.js",
        "./wwwroot/lib/jquery/dist/jquery.min.js"
    ])
        .pipe(gulpConcat('compiled-libs.js'))
        .pipe(gulp.dest('./wwwroot/dist/js'));
});

function compile(watch) {
    // if in the future we have multiple entry points perhaps create a JSON map with all the script names and _.map them to load
    var bundler = watchify(browserify('./wwwroot/assets/js/app.jsx', { debug: true }).transform(babel));

    function rebundle() {
        bundler.bundle()
            .on('error', function (err) {
                console.error(err); this.emit('end');
            })
            .pipe(source('compiled-main.js'))
            .pipe(buffer())
            .pipe(sourcemaps.init({ loadMaps: true }))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest('./wwwroot/dist/js'));
    }

    if (watch) {
        bundler.on('update', function () {
            console.log('-> bundling...');
            rebundle();
        });
    }

    rebundle();
}

gulp.task('compile-less', function () {
    gulp.src("./wwwroot/assets/less/style.less")
        .pipe(plumber())
        .pipe(less())
        .pipe(gulp.dest('./wwwroot/dist/css'))
        .pipe(cssmin())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./wwwroot/dist/css'));
});

gulp.task('browserify', function () {
    return compile();
});

gulp.task('watch.browserify', function () {
    return compile(true);
});

gulp.task('watch.less', function () {
    gulp.watch('./wwwroot/assets/less/**/*.less', ['compile-less']);
});

gulp.task('watch.js', function () {
    gulp.watch('./wwwroot/assets/js/*.jsx', ['browserify']);
    gulp.watch('./wwwroot/assets/js/*.js', ['browserify']);
});

gulp.task('watch', ['watch.js', 'watch.less']);

gulp.task('build-spa', ['compile-less', 'browserify', 'concat-libs']);