﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ImageResizerUpload.Home.ViewModels;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;
using ImageResizer.Utilities;
using Microsoft.Extensions.Logging;
using System.IO;
using ImageMagick;

namespace ImageResizerUpload.Home.Services.Implementation
{
  public class ImageService: IImageService
  {
    private readonly AppSettings _settings;
    private readonly ILogger _logger;
    private readonly IStringLocalizer<SharedResource> _localizer;

    public ImageService(IStringLocalizer<SharedResource> localizer, IOptions<AppSettings> settings, ILogger logger = null)
    {
      _settings = settings.Value;
      _localizer = localizer;
      _logger = logger;
    }

    public string Resize(ContentViewModel image, string destination)
    {
      CheckSource(image.Source);

      var extension = ImageUtilities.ParseImageExtension(image.Source.ContentType);
      var fileName = ImageUtilities.GenerateImageName(extension);

      if (image.ResizeFactor < 1 || image.ResizeFactor > 100)
        throw new ArgumentOutOfRangeException(_localizer["ExceptionImageResizeValue"]);

      double resize = (double)image.ResizeFactor / 100;

      using (var source = new MagickImage(image.Source.OpenReadStream()))
      {
        var resizedWidth = (int)Math.Ceiling(source.Width * resize);
        var resizedHeight = (int)Math.Ceiling(source.Height * resize);

        MagickGeometry size = new MagickGeometry(resizedWidth, resizedHeight);

        source.Strip();
        source.Resize(size);
        source.Write(Path.Combine(destination, fileName));
        source.Dispose();
      }

      return fileName;
    }

    public async Task<string> Upload(ContentViewModel image, string destination)
    {
      CheckSource(image.Source);

      var extension = ImageUtilities.ParseImageExtension(image.Source.ContentType);
      var fileName = ImageUtilities.GenerateImageName(extension);

      using (var fs = new FileStream(Path.Combine(destination, fileName), FileMode.Create)) // upscale any exception thrown here without try-catch
      {
        await image.Source.CopyToAsync(fs);
        fs.Flush();
      }

      return fileName;
    }

    private void CheckSource(IFormFile source)
    {
      // move these validations to ViewModel also
      // TODO migrate conditional validator utility for View Models and implement these
      if (source == null)
        throw new ArgumentNullException(_localizer["ExceptionImageEmpty"]);

      if (source.Length == 0)
        throw new ArgumentNullException(_localizer["ExceptionImageEmpty"]);

      if (ImageUtilities.ConvertImageLengthToMB(source.Length) > _settings.MaxFileSize)
        throw new ArgumentOutOfRangeException(_localizer["ExceptionImageTooBig"]);

      if (!_settings.SupportedFormats.Contains(source.ContentType))
        throw new ArgumentException(_localizer["ExceptionImageNotSupported"]);
    }
  }
}
