﻿using ImageResizerUpload.Home.ViewModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageResizerUpload.Home.Services
{
  public interface IImageService
  {
    Task<string> Upload(ContentViewModel image, string destination);
    string Resize(ContentViewModel image, string destination);
  }
}
