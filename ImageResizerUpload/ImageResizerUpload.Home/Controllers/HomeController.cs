﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ImageResizerUpload.Home.ViewModels;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Microsoft.Extensions.Localization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ImageResizerUpload.Home.Controllers
{
  public class HomeController : Controller
  {
    private readonly AppSettings _settings;
    private readonly IStringLocalizer<SharedResource> _localizer;
    private readonly IStringLocalizer<HomeController> _homeLocalizer;

    public HomeController(IOptions<AppSettings> settings, IStringLocalizer<SharedResource> localizer, IStringLocalizer<HomeController> homeLocalizer)
    {
      _settings = settings.Value;
      _localizer = localizer;
      _homeLocalizer = homeLocalizer;
    }

    public IActionResult Index()
    {
      ViewData["ApplicationName"] = _localizer["ApplicationName"];
      return View(new IndexViewModel
      {
        AppSettings = _settings
      });
    }

    public IActionResult Error()
    {
      ViewData["ApplicationName"] = _localizer["ApplicationName"];
      ViewData["GenericError"] = _homeLocalizer["GenericError"];
      return View(new IndexViewModel
      {
        AppSettings = _settings
      });
    }
  }
}
