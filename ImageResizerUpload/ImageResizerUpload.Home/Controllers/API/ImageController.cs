﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using ImageResizerUpload.Home.Services;
using ImageResizerUpload.Home.ViewModels;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Hosting;
using System.IO;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ImageResizerUpload.Home.Controllers.API
{
  [AllowAnonymous, Route("~/api/[controller]")]
  public class ImageController : Controller
  {
    private readonly AppSettings _settings;
    private readonly IStringLocalizer<SharedResource> _localizer;
    private readonly IStringLocalizer<ImageController> _imageLocalizer;
    private readonly IImageService _service;
    private readonly IHostingEnvironment _environment;

    private string _location;

    public ImageController(IOptions<AppSettings> settings, IHostingEnvironment environment, IStringLocalizer<SharedResource> localizer, IStringLocalizer<ImageController> imageLocalizer, IImageService service)
    {
      _settings = settings.Value;
      _service = service;
      _localizer = localizer;
      _imageLocalizer = imageLocalizer;
      _environment = environment;

      _location = Path.Combine(_environment.WebRootPath, _settings.UploadWebFolder);
    }

    [AllowAnonymous]
    [HttpPost, Route("~/api/[controller]/upload")]
    public async Task<Dictionary<string, string>> Upload(IFormFile source, int resize)
    {
      var content = new ContentViewModel()
      {
        ResizeFactor = resize,
        Source = source
      };

      string fileName = await _service.Upload(content, _location);
      return new Dictionary<string, string> { { "url", $"/{_settings.UploadWebFolder}/{fileName}" } };
    }

    [AllowAnonymous]
    [HttpPost, Route("~/api/[controller]/resize")]
    public Dictionary<string, string> Resize(IFormFile source, int resize)
    {
      var content = new ContentViewModel()
      {
        ResizeFactor = resize,
        Source = source
      };

      string fileName = _service.Resize(content, _location);
      return new Dictionary<string, string> { { "url", $"/{_settings.UploadWebFolder}/{fileName}" } };
    }
  }
}
