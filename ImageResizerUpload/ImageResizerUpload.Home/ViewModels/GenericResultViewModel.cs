﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageResizerUpload.Home.ViewModels
{
  public class GenericResultViewModel
  {
    public int ErrorCode { get; set; }
    public bool UserError { get; set; }
    public string ErrorMessage { get; set; }
    public string StackTrace { get; set; }
    public object Results { get; set; }
  }
}
