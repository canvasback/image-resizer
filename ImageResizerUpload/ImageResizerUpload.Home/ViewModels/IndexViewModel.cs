﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageResizerUpload.Home.ViewModels
{
  public class IndexViewModel
  {
    public JsonSerializerSettings Settings { get; set; }
    public AppSettings AppSettings { get; set; }
  }
}
