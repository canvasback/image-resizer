﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ImageResizerUpload.Home.ViewModels
{
  public class ContentViewModel
  {
    [Required]
    public IFormFile Source { get; set; }

    public int ResizeFactor { get; set; } = 100;
  }
}
