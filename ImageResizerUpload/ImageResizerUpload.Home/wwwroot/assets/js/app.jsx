﻿import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';
import $ from 'jquery';

import SimpleUploader from './uploader.jsx';

window.React = React; // console utils. remove for production

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() { } // whenever the App is rendered to the DOM for the first time

    componentWillUnmount() { } // whenever the DOM produced by the App is removed

    render() {
        return (
            <div>
                <SimpleUploader />
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('reactAppRoot'));