﻿import React from 'react';

export class Loader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() { } // whenever the SimpleUploader is rendered to the DOM for the first time

    componentWillUnmount() { } // whenever the DOM produced by the SimpleUploader is removed

    render() {
        return (
            <div className={`o-loader ${this.props.isLoading ? 'is-loading' : ''}`}>
                <div className="o-loader__bar"></div>
            </div>
        );
    }
}