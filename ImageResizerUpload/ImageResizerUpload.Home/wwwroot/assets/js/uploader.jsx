﻿import React from 'react';
import Resources from './localization.jsx';
import { Loader } from './uicomponents.jsx';
import _ from 'lodash';

class Uploader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fileData: null,
            name: null,
            size: 0,
            type: null,
            resize: 100,
            isResizedOnServer: false,
            errorField: null,
            errorMessage: null
        };

        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleFileChange = this.handleFileChange.bind(this);
        this.handleFileResizeChange = this.handleFileResizeChange.bind(this);
        this.handleFileIsResizedOnServerChange = this.handleFileIsResizedOnServerChange.bind(this);
    }

    resetValidationState() {
        this.setState({
            errorField: null,
            errorMessage: null
        })
    }

    stateIsValid() {
        if (_.isEmpty(this.state.name)) {
            this.setState({
                errorField: 'fileInput',
                errorMessage: Resources.fileInputEmpty // 'An image is required.'
            });

            return false;
        }

        if (!_.includes(window.imageUploader.staticData.supportedFormats, this.state.type)) {
            this.setState({
                errorField: 'fileInput',
                errorMessage: Resources.fileInputNotSupported // 'This image format is not supported.'
            });

            return false;
        }

        let sizeInMB = (this.state.size / (1024 * 1024)).toFixed(2);
        if (sizeInMB > window.imageUploader.staticData.maxFileSize) {
            this.setState({
                errorField: 'fileInput',
                errorMessage: Resources.fileInputTooBig // 'This image is too big.'
            });

            return false;
        }

        if (_.isNull(this.state.resize) || _.isUndefined(this.state.resize)) {
            this.setState({
                errorField: 'fileResize',
                errorMessage: Resources.resizeFactorMissing // 'A resize factor is required.'
            });

            return false;
        }

        if (!_.isInteger(this.state.resize)) {
            this.setState({
                errorField: 'fileResize',
                errorMessage: Resources.resizeFactorMissing // 'A resize factor is required.'
            });

            return false;
        }

        if (this.state.resize < 1 || this.state.resize > 100) {
            this.setState({
                errorField: 'fileResize',
                errorMessage: Resources.resizeFactorOutOfBounds // 'A resize factor must be between 1 and 100.'
            });

            return false;
        }

        return true;
    }

    handleFormSubmit(event) {
        console.log('Changing handleFormSubmit()');

        event.preventDefault();

        this.resetValidationState();
        if (!this.stateIsValid())
            return;

        this.props.onUploaderSubmit({
            fileData: this.state.fileData,
            resize: this.state.resize,
            isResizedOnServer: this.state.isResizedOnServer
        });
    }

    handleFileChange(event) {
        console.log('Changing handleFileChange()');
        this.resetValidationState();
        let file = event.target.files[0];

        if (!file)
            return;

        this.setState({
            name: file.name,
            size: file.size,
            type: file.type,
            fileData: file
        });
    }

    handleFileResizeChange(event) {
        console.log('Changing handleFileResizeChange()');
        this.resetValidationState();
        this.setState({
            resize: parseInt(event.target.value)
        });
    }

    handleFileIsResizedOnServerChange(event) {
        console.log('Changing handleFileIsResizedOnServerChange()');
        this.resetValidationState();
        this.setState({
            isResizedOnServer: event.target.checked
        });
    }

    componentDidMount() { } // whenever the SimpleUploader is rendered to the DOM for the first time

    componentWillUnmount() { } // whenever the DOM produced by the SimpleUploader is removed

    render() {
        return (
            <div className="c-uploader">
                <div>
                    <h3>{Resources.uploaderTitle}</h3>
                </div>
                <div className="is-disclaimer">
                    <span>{Resources.formatString(Resources.uploaderDisclaimer, window.imageUploader.staticData.maxFileSize)}</span>
                    <span>{Resources.formatString(Resources.uploaderDisclaimer2, window.imageUploader.staticData.supportedFormats.join(', '))}</span>
                </div>
                <div className="c-uploader__form-area">
                    <form id="fileUploadForm" onSubmit={this.handleFormSubmit}>
                        <div className={`o-inputblock ${this.state.errorField == 'fileInput' ? 'has-error' : ''}`}>
                            <label htmlFor="fileInput">{Resources.inputChooseFile}</label>
                            <input id="fileInput" className="o-input o-input--file" type="file" onChange={this.handleFileChange} />
                            {this.state.errorField == 'fileInput' && 
                                <span className="has-error">{this.state.errorMessage}</span>
                            }
                        </div>
                        <div className={`o-inputblock ${this.state.errorField == 'fileResize' ? 'has-error' : ''}`}>
                            <label htmlFor="fileResize">{Resources.inputChooseResize}</label>
                            <input id="fileResize" className="o-input" type="number" min="1" max="100" value={this.state.resize} onChange={this.handleFileResizeChange} />
                            {this.state.errorField == 'fileResize' &&
                                <span className="has-error">{this.state.errorMessage}</span>
                            }
                        </div>
                        <div className="o-inputblock">
                            <label htmlFor="fileResizeOnServer" className="is-inline">{Resources.inputResizeOnServer}</label>
                            <input id="fileResizeOnServer" className="o-input" type="checkbox" defaultChecked={this.state.isResizedOnServer} onChange={this.handleFileIsResizedOnServerChange} />
                        </div>
                        <div className="o-inputblock c-uploader__button">
                            <button type="submit" className={this.props.isUploading ? 'is-disabled' : ''} disabled={this.props.isUploading}>{Resources.buttonUpload}</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

class Previewer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() { } // whenever the SimpleUploader is rendered to the DOM for the first time

    componentWillUnmount() { } // whenever the DOM produced by the SimpleUploader is removed

    render() {
        const hasError = this.props.uploadState.hasError;
        const errorMessage = this.props.uploadState.errorMessage;
        const path = this.props.uploadState.path;

        return (
            <div className="c-previewer">
                {hasError &&
                    <div className="c-previewer__error">
                        <h2>{errorMessage}</h2>
                    </div>
                }
                {!hasError && path &&
                    <div className="c-previewer__image">
                        <img src={path}></img>
                    </div>
                }
            </div>
        );
    }
}

export default class SimpleUploader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isUploading: false,
            uploadState: {
                path: null,
                hasError: false,
                errorMessage: null
            }
        };

        this.handleUploaderSubmit = this.handleUploaderSubmit.bind(this);
    }

    uploadAnResizeRemote(uploader) {
        this.setState({
            isUploading: true
        });

        let data = new FormData();
        data.append('source', uploader.fileData); // for .net web api the form field name must match the API signature!
        data.append('resize', uploader.resize);

        let ajaxObj = {
            url: uploader.isResizedOnServer ? '/api/image/resize' : '/api/image/upload',
            contentType: false,
            processData: false,
            data: data,
            method: 'POST',
            cache: false
        };

        let request = $.ajax(ajaxObj);

        request
            .then((data, textStatus, jqXHR) => {
                this.setState({
                    uploadState: {
                        path: data.url,
                        hasError: false,
                        errorMessage: null
                    }
                });
            },
            (jqXHR, textStatus, errorThrown) => {
                this.setState({
                    uploadState: {
                        path: null,
                        hasError: true,
                        errorMessage: jqXHR.responseJSON.ErrorMessage
                    }
                });
            })
            .always(() => {
                this.setState({
                    isUploading: false
                });
            });
    }

    uploadAnResizeLocally(uploader) {
        this.setState({
            isUploading: true
        });

        let img = document.createElement('img');
        let canvas = document.createElement('canvas');
        let reader = new FileReader();

        reader.onload = (e) => {
            e.preventDefault();

            img.src = e.target.result;

            return true;
        }

        img.onload = (e) => {
            e.preventDefault();

            let resize = uploader.resize / 100;
            let ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0);

            canvas.width = Math.round(img.width * resize);
            canvas.height = Math.round(img.height * resize);

            ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

            let fileData = this.extractResizedCanvasFile(canvas, uploader);
            let uploadResizedForm = {
                fileData: fileData,
                resize: uploader.resize,
                isResizedOnServer: false
            }
            this.uploadAnResizeRemote(uploadResizedForm);

            return true;
        }

        reader.readAsDataURL(uploader.fileData);
    }

    extractResizedCanvasFile(canvas, uploader) {
        let dataUrl = canvas.toDataURL(uploader.fileData.type, 1);
        let dataUrlAsFile = this.dataUrlAsFileFormat(dataUrl);
        return dataUrlAsFile;
    }

    dataUrlAsFileFormat(dataUrl) {
        // credits from https://github.com/miohtama/Krusovice/blob/master/src/tools/resizer.js#L51
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs

        let byteString;
        if (dataUrl.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataUrl.split(',')[1]);
        else
            byteString = unescape(dataUrl.split(',')[1]);
        

        // separate out the mime component
        let mimeString = dataUrl.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to an ArrayBuffer
        let ab = new ArrayBuffer(byteString.length);
        let ia = new Uint8Array(ab);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        // write the ArrayBuffer to a blob, and you're done
        try {
            return new Blob([ab], { type: mimeString });
        } catch (e) {
            // The BlobBuilder API has been deprecated in favour of Blob, but older
            // browsers don't know about the Blob constructor
            // IE10 also supports BlobBuilder, but since the `Blob` constructor
            //  also works, there's no need to add `MSBlobBuilder`.
            var BlobBuilder = window.WebKitBlobBuilder || window.MozBlobBuilder;
            var bb = new BlobBuilder();
            bb.append(ab);
            return bb.getBlob(mimeString);
        }
    }

    handleUploaderSubmit(uploader) {
        if (uploader.isResizedOnServer)
            this.uploadAnResizeRemote(uploader);
        else
            this.uploadAnResizeLocally(uploader);
    }

    componentDidMount() { } // whenever the SimpleUploader is rendered to the DOM for the first time

    componentWillUnmount() { } // whenever the DOM produced by the SimpleUploader is removed

    render() {
        return (
            <section className="o-panel o-panel--fullwidth has-visible-dividers">
                <Loader isLoading={this.state.isUploading} />
                <Uploader onUploaderSubmit={this.handleUploaderSubmit} isUploading={this.state.isUploading} />
                {!this.state.isUploading &&
                    <Previewer uploadState={this.state.uploadState} />
                }
            </section>
        );
    }
}