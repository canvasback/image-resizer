﻿import LocalizedStrings from 'react-localization';

module.exports = new LocalizedStrings({
    en: {
        uploaderTitle: 'Pick an image you would like to send and resize!',
        uploaderDisclaimer: 'Note: The image size is limited up to {0}MB. ',
        uploaderDisclaimer2: 'And the format should be one of these: {0}.',
        fileInputEmpty: 'An image is required!',
        fileInputNotSupported: 'This image format is not supported.',
        fileInputTooBig: 'This image is too big.',
        resizeFactorMissing: 'A resize factor is required.',
        resizeFactorOutOfBounds: 'A resize factor must be between 1 and 100.',
        inputChooseFile: 'Choose an image to send:',
        inputChooseResize: 'Pick the resize factor (1%, 100%):',
        inputResizeOnServer: 'Resize image on server: ',
        buttonUpload: 'Upload my Image'
    },
    pt: {
        uploaderTitle: 'Escolha uma imagem que gostaria de enviar e redimensionar!',
        uploaderDisclaimer: 'Nota: O tamanho das imagens está limitado a {0}MB. ',
        uploaderDisclaimer2: 'E o formato deverá ser um dos seguintes: {0}.',
        fileInputEmpty: 'É necessário definir uma imagem!',
        fileInputNotSupported: 'O formato da imagem não é suportado.',
        fileInputTooBig: 'A imagem é demasiado grande.',
        resizeFactorMissing: 'É necessário definir um valor de redimensionamento.',
        resizeFactorOutOfBounds: 'A percentagem de redimensionamento deverá ser entre 1 e 100.',
        inputChooseFile: 'Escolha uma imagem para enviar:',
        inputChooseResize: 'Escolha um factor de redimensionamento (1%, 100%):',
        inputResizeOnServer: 'Redimensionar no servidor? ',
        buttonUpload: 'Submeter a minha Imagem'
    }
});

