﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Diagnostics;
using System.Net;
using Newtonsoft.Json;
using ImageResizerUpload.Home.ViewModels;
using Microsoft.Net.Http.Headers;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.AspNetCore.Http;
using ImageResizerUpload.Home.Services.Implementation;
using ImageResizerUpload.Home.Services;
using Microsoft.AspNetCore.Http.Features;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Options;

namespace ImageResizerUpload.Home
{
  public class Startup
  {
    public Startup(IHostingEnvironment env)
    {
      var builder = new ConfigurationBuilder()
          .SetBasePath(env.ContentRootPath + "/Config")
          .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
          .AddEnvironmentVariables();

      Configuration = builder.Build();
    }

    public IConfigurationRoot Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddOptions();
      services.Configure<AppSettings>(Configuration.GetSection(nameof(AppSettings)));
      
      InitializeAppServices(services);

      // Add framework services.
      services.AddLocalization(options => options.ResourcesPath = "Resources");
      services.AddAntiforgery();
      services.AddDirectoryBrowser();
      services.AddApplicationInsightsTelemetry(Configuration);
      services
        .AddMvc()
        .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
        .AddDataAnnotationsLocalization();

      services.Configure<RequestLocalizationOptions>(
        options =>
        {
          var supportedCultures = new List<CultureInfo>
          {
            new CultureInfo("en-US"),
            new CultureInfo("pt-PT"),
          };
          options.DefaultRequestCulture = new RequestCulture(culture: "en-US", uiCulture: "en-US");
          options.SupportedCultures = supportedCultures;
          options.SupportedUICultures = supportedCultures;
        });
    }

    private void InitializeAppServices(IServiceCollection services)
    {
      #region Initialize repositories
      #endregion Initialize repositories

      #region Initialize Services
      services.AddScoped<IImageService, ImageService>();
      #endregion Initialize Services

      services.AddSingleton<IConfiguration>(Configuration);
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
    {
      loggerFactory.AddConsole(Configuration.GetSection("Logging"));
      loggerFactory.AddDebug();

      var localizationOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
      app.UseRequestLocalization(localizationOptions.Value);

      if (env.EnvironmentName.Contains("Dev"))
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseExceptionHandler("/Home/Error");
        app.UseStatusCodePagesWithReExecute("/Home/Error/{0}");
      }

      app.UseExceptionHandler(
        builder =>
        {
          builder.Run(
            async context =>
            {
              var error = context.Features.Get<IExceptionHandlerFeature>();
              if (error != null)
              {
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)ParseStatusCodeByException(error.Error);

                var genericResult = new GenericResultViewModel
                {
                  ErrorCode = context.Response.StatusCode, // improve to contemplate specific error cases
                  UserError = context.Response.StatusCode == (int)HttpStatusCode.BadRequest,
                  StackTrace = env.EnvironmentName.Contains("Dev") ? error.Error.StackTrace : "",
                  ErrorMessage = error.Error.Message
                };

                await context.Response.WriteAsync(JsonConvert.SerializeObject(genericResult)).ConfigureAwait(false);
              }
            });
        });

      app.UseStaticFiles(new StaticFileOptions
      {
        OnPrepareResponse = ctx =>
        {
          const int durationInSeconds = 60 * 60 * 24;
          ctx.Context.Response.Headers[HeaderNames.CacheControl] =
              "public,max-age=" + durationInSeconds;
        }
      });

      app.UseStaticFiles(new StaticFileOptions()
      {
        FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot", "uploads")),
        RequestPath = new PathString("/uploads")
      });

      app.UseDirectoryBrowser(new DirectoryBrowserOptions()
      {
        FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot", "uploads")),
        RequestPath = new PathString("/uploads")
      });

      app.UseMvc(routes => routes.MapRoute(
        name: "default",
        template: "{controller=Home}/{action=Index}/{id?}"));
    }

    private HttpStatusCode ParseStatusCodeByException(Exception e)
    {
      if (e is ArgumentException || e is ArgumentOutOfRangeException || e is ArgumentNullException)
        return HttpStatusCode.BadRequest;

      if (e is NotSupportedException)
        return HttpStatusCode.MethodNotAllowed;

      if (e is NotImplementedException)
        return HttpStatusCode.NotImplemented;

      if (e is UnauthorizedAccessException)
        return HttpStatusCode.Unauthorized;

      return HttpStatusCode.InternalServerError;
    }
  }
}
