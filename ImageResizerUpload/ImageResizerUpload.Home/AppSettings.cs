﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageResizerUpload.Home
{
  public class AppSettings
  {
    public string UploadWebFolder { get; set; }
    public string[] SupportedFormats { get; set; }
    public int MaxFileSize { get; set; }
  }
}
