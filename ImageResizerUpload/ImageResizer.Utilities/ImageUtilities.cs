﻿using System;
using System.Collections.Generic;

namespace ImageResizer.Utilities
{
  public class ImageUtilities
  {
    public static double ConvertImageLengthToMB(long bytes) => ((bytes / 1024f) / 1024f);

    public static string GenerateImageName(string extension) => (Guid.NewGuid() + extension); 

    public static string ParseImageExtension(string mimeType)
    {
      switch (mimeType)
      {
        case "image/png":
          return ".png";
        case "image/jpeg":
          return ".jpg";
        case "image/bmp":
          return ".bmp";
        default:
          return null;
      }
    }
  }
}
