**Image Uploader and Resizer**

A simple image uploader written with [.NET Core Web API](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-web-api) and [ReactJS](https://facebook.github.io/react/).
It also supports both server-side (using [Magick.NET](https://magick.codeplex.com/) and client-side image processing (resizing only).

* Quick summary
> Built as my very first ReactJS single-page application!
> Store knowledge on the challanges of submitting multipart data to .NET Core WEB API. As well as processing images on server-side without System.Drawing. For frontend learn how ReactJS works as well as using HTML5 Canvas to perform client-side image processing.

* Version
> 0.0.10

* Challanges
> 1. Handle server-side image processing without System.Drawing since it isn't portable. Try out the recent work made on portable Processing Libraries.
> 2. Build a ReactJS SPA using Browserify, Babel and Gulp (and several other things. Instead of using Webpack given my current experience. Also analyze the transpiling of ES2016 to older versions as well as new language features!
> 3. Implement HTML5 Canvas and FileReader for in-memory image processing on client-side and check backward compatibilities.
> 4. How Web API handles multipart data in a pure stateless way without ASP.NET

* Future Work
> 1. Use inmemory database or other DB Engine to store user's uploaded files. Using EFCore as ORM!
> 2. Implement Identity to add login feature and working area.
> 3. Store uploaded images relationship with user.
> 4. Use Flux pattern
> 5. Improve UI adding the original size of image and the resulting one

## How do I get set up? ##

** Summary of set up **

The project was written in Visual Studio 2017 (need to check backward compatibility with Visual Studio 2015)

** Requirements **

* NPM
* Bower
* Visual Studio 2017
* .NET Core 1.1

** Dependencies (main ones)**

* NodeJS/NPM
* Bower
* Gulp/Babel/Browserify (... and more)
* ReactJS (react and react-dom)
* JQuery
* Magick.NET
* .NET Core

** Configuration **

1. The repository is public and available [https://canvasback@bitbucket.org/canvasback/image-resizer.git](https://canvasback@bitbucket.org/canvasback/image-resizer.git)
2. Open the solution
3. Build the solution
4. Make sure NPM and Bower dependencies are installed (if not wait a little bit of force them with 'Package Restore')
5. Under Task Planner run the Gulp's 'build-spa' task

** Execution and Deployment **

1. Pick an Execution profile
2. Run the project and open it under a browser

### Who do I talk to? ###
- [Tiago Pateiro](https://bitbucket.org/canvasback/)